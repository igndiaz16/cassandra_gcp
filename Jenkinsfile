def CASSANDRA_IP=[]
def CASSANDRA_NETWORK
def CASSANDRA_CLUSTER
def CASSANDRA_TYPE
pipeline {
    agent any 
    parameters {
        string(name: 'PROJECTO', defaultValue: 'my-own-project-252421', description: 'Nombre Projecto Cloud')
        string(name: 'CLUSTER_NAME', defaultValue: 'Test Cluster', description: 'Nombre del Cluster Cassandra')
        choice(name: 'ENV', choices: ['prod', 'dev'], description: 'Ambiente')
        string(name: 'NODOS', defaultValue: '1', description: 'Cantidad de Nodos Cluster')
        choice(name: 'SNITCH', choices: ['SimpleSnitch', 'GossipingPropertyFileSnitch', 'GoogleCloudSnitch'], description: 'Tipo de Snitch Cluster')
    }
    stages {
        stage ('Creación Máquinas') {
            steps {

            // SET PROJECTO
            sh "gcloud config set project ${params.PROJECTO}" 
                
                script {

                // SET TIPO DE MAQUINA
                if ("${params.ENV}" == 'prod') {CASSANDRA_TYPE='n1-standard-8' } else {CASSANDRA_TYPE='n1-standard-4'} 

                // CREACION DE MAQUINAS
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++) {
                sh "gcloud beta compute --project=${params.PROJECTO} instances create cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --machine-type=${CASSANDRA_TYPE} --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --service-account=812385867631-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --tags=http-server,https-server,cassandra-cluster-db-tier --image=debian-9-stretch-v20200420 --image-project=debian-cloud --boot-disk-size=10GB --boot-disk-type=pd-standard --boot-disk-device-name=cassandra-${params.ENV}-${loopIndex} --create-disk=mode=rw,size=100,type=projects/${params.PROJECTO}/zones/us-central1-a/diskTypes/pd-ssd,name=cassandra-${params.ENV}-disk-${loopIndex},device-name=cassandra-${params.ENV}-disk-${loopIndex} --reservation-affinity=any" 
                }

                // REGLAS DE FIREWALL
                sh """
                gcloud compute --project=${params.PROJECTO} firewall-rules create cassandra-cluster-db-tcp-7000-7001 --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:7000-7001 --source-tags=cassandra-cluster-db-tier --target-tags=cassandra-cluster-db-tier

                gcloud compute --project=${params.PROJECTO} firewall-rules create cassandra-cluster-db-tcp-7199 --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=tcp:7199 --source-tags=cassandra-cluster-db-tier --target-tags=cassandra-cluster-db-tier

                gcloud compute --project=${params.PROJECTO} firewall-rules create cassandra-cluster-icmp --direction=INGRESS --priority=1000 --network=default --action=ALLOW --rules=icmp --source-tags=cassandra-cluster-db-tier --target-tags=cassandra-cluster-db-tier

                gcloud compute --project=${params.PROJECTO} instance-groups unmanaged create cassandra-cluster --zone=us-central1-a
                """

                // ADICIONAR AL GRUPO DE INSTANCIAS Y CAPTURA DE IP INTERNA
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++) {

                sh "gcloud compute --project=${params.PROJECTO} instance-groups unmanaged add-instances cassandra-cluster --zone=us-central1-a --instances=cassandra-${params.ENV}-${loopIndex}"

                CASSANDRA_NETWORK= sh(script: "gcloud compute instances describe cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --format='value(networkInterfaces.networkIP)'", returnStdout: true)

                echo CASSANDRA_NETWORK

                CASSANDRA_IP.add(CASSANDRA_NETWORK.trim())           
                }
                       }   
            } 
        }
        stage('Instalacion Cassandra') {
            steps {
                script {
                
                // PROVISIONAMIENTO E INSTALACION DE CASSANDRA
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                sh """ 
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo apt-get install -y apt-transport-https'
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo apt-get update'
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'echo "deb https://downloads.apache.org/cassandra/debian 311x main" | sudo tee -a /etc/apt/sources.list.d/cassandra.sources.list'
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'curl https://downloads.apache.org/cassandra/KEYS | sudo apt-key add -'
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo apt-get update'
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo apt-get -y install cassandra'
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo service cassandra stop'
                """
                }
                       }
            }  
        }
        stage('Modificaciones Nodo') {
            steps {
                script {
                
                // UNION DE IPS INTERNAS CLUSTER
                CASSANDRA_CLUSTER=CASSANDRA_IP.join(",")

                //CAMBIOS EN CASSANDRA.YAML PARA CADA NODO
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                
                //ELIMINACION DE ARCHIVO AL CAMBIAR DE SNITCH
                if ("${params.SNITCH}" == 'GossipingPropertyFileSnitch'){
                sh "gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command 'sudo rm /etc/cassandra/cassandra-topology.properties'"  
                }

                sh """
                CASSANDRA_NETWORK=\$(gcloud compute instances describe cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --format='value(networkInterfaces.networkIP)')
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo sed -i 's/rpc_address: localhost/rpc_address: 0.0.0.0/gI' /etc/cassandra/cassandra.yaml"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo sed -i 's/localhost/\$CASSANDRA_NETWORK/gI' /etc/cassandra/cassandra.yaml"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo sed -i 's/# broadcast_rpc_address: 1.2.3.4/broadcast_rpc_address: \$CASSANDRA_NETWORK/gI' /etc/cassandra/cassandra.yaml"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo sed -i 's/127.0.0.1/"${CASSANDRA_CLUSTER}"/gI' /etc/cassandra/cassandra.yaml"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo sed -i 's/Test Cluster/${params.CLUSTER_NAME}/gI' /etc/cassandra/cassandra.yaml"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo sed -i 's/SimpleSnitch/${params.SNITCH}/gI' /etc/cassandra/cassandra.yaml"
                """
                }
            }
            }
        }  
        stage('Inicio de Servicio & Validación') {
            steps {
                 script {
                // INICIO SERVICIO
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                sh """
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sudo service cassandra start"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "sleep 20"
                """
                }

                //VALIDACIÓN FUNCIONAMIENTO
                for (loopIndex=0; loopIndex < Integer.parseInt("${params.NODOS}");loopIndex++){
                sh """ 
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "nodetool status"
                gcloud compute ssh cassandra-${params.ENV}-${loopIndex} --zone=us-central1-a --command "cqlsh -e 'describe keyspaces;'"
                """
                }
                }  
            }
        }    
    }
}
